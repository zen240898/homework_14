#include <iostream>
#include <string>

int main()
{
    std::string homework = "Homework_14";
    std::cout << homework << "\n";
    std::cout << homework.length() << "\n";
    std::cout << "First letter: " << homework[0] << "\n";
    int LastLetter = homework.length() - 1;
    std::cout << "Last letter: " << homework[LastLetter] << "\n";
}
